from sqlalchemy import create_engine


engine = create_engine('sqlite:///college.db', echo=True)

conn = engine.connect() # creates the database on the disk
print(conn)